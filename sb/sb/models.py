from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length=200)
    doc = models.CharField(max_length=200)
    email = models.EmailField()
    phone = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def get_address(self):
        return Address.objects.filter(customer_id=self.id)

    def get_site(self):
        return Site.objects.filter(customer_id=self.id)


class Site(models.Model):
   customer_id = models.ForeignKey(Customer)
   url = models.URLField()


class Address(models.Model):
    BRAZIL_STATES = (('', u''),
                     ('AC', u'Acre'),
                     ('AL', u'Alagoas'),
                     ('AP', u'Amapá'),
                     ('AM', u'Amazonas'),
                     ('BA', u'Bahia'),
                     ('CE', u'Ceará'),
                     ('DF', u'Distrito Federal'),
                     ('ES', u'Espírito Santo'),
                     ('GO', u'Goiás'),
                     ('MA', u'Maranhão'),
                     ('MT', u'Mato Grosso'),
                     ('MS', u'Mato Grosso do Sul'),
                     ('MG', u'Minas Gerais'),
                     ('PA', u'Pará'),
                     ('PB', u'Paraíba'),
                     ('PR', u'Paraná'),
                     ('PE', u'Pernambuco'),
                     ('PI', u'Piauí'),
                     ('RJ', u'Rio de Janeiro'),
                     ('RN', u'Rio Grande do Norte'),
                     ('RS', u'Rio Grande do Sul'),
                     ('RO', u'Rondônia'),
                     ('RR', u'Roraima'),
                     ('SC', u'Santa Catarina'),
                     ('SP', u'São Paulo'),
                     ('SE', u'Sergipe'),
                     ('TO', u'Tocantins'))

    customer_id = models.ForeignKey(Customer)
    street = models.CharField(max_length=200)
    number = models.IntegerField()
    district = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=2,
                             choices=BRAZIL_STATES)