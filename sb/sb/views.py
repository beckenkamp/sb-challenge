from django.http import HttpResponseRedirect
from django.views import generic

from .forms import CustomerForm, AddressFormSet, SiteFormSet
from .models import Customer, Address, Site


class IndexView(generic.ListView):
    template_name = 'sb/index.html'
    context_object_name = 'customer_list'

    def get_queryset(self):
        return Customer.objects.order_by('-date')


class CustomerCreateView(generic.CreateView):
    template_name = 'sb/customer_form.html'
    model = Customer
    form_class = CustomerForm
    success_url = '/'

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        address_form = AddressFormSet()
        site_form = SiteFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  address_form=address_form,
                                  site_form=site_form))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        address_form = AddressFormSet(self.request.POST)
        site_form = SiteFormSet(self.request.POST)
        if (form.is_valid() and address_form.is_valid() and
            site_form.is_valid()):
            return self.form_valid(form, address_form, site_form)
        else:
            return self.form_invalid(form, address_form, site_form)

    def form_valid(self, form, address_form, site_form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        self.object = form.save()
        address_form.instance = self.object
        address_form.save()
        site_form.instance = self.object
        site_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, address_form, site_form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form,
                                  address_form=address_form,
                                  site_form=site_form))
