from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from .models import Customer, Address, Site


class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        exclude = ['date']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })


AddressFormSet = inlineformset_factory(Customer, Address, exclude=['date'], form=CustomerForm)
SiteFormSet = inlineformset_factory(Customer, Site, exclude=['date'], form=CustomerForm)