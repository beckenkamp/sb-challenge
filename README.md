# Desafio Site Blindado

Este teste foi feito para podermos avaliar o seu potencial como programador. Esteja a vontade de fazer perguntas, questionar, propor alternativas, usar ferramentas e frameworks do seu conhecimento para resolver o problema proposto.

Critérios de Avaliação
--------------------
Os seguintes aspectos do seu projeto serão avaliados, além de quão longe você prosseguiu no caminho da força:

* Agilidade;
* Legibilidade;
* Escopo;
* Organização do código;
* Padrões de projeto;
* Existência e quantidade de bugs e gambiarras;
* Cobertura da aplicação com testes;

### Problema 1

> O pessoal de produtos detectou a seguinte necessidade: o site do portal não tem uma boa usabilidade e foi decidido refazê-lo. Para dar início a esse projeto, foi decidido começar pela tela de cadastro do usuário. Durante o cadastro no Site Blindado, o usuário deve informar seus dados de contato, bem como adicionar os domínios aos quais ele deseja que sejam realizados scans.

  - Desenvolver um sistema simples que permita o cadastro de clientes com nome, endereço, CPF, email, telefone e domínios que deverão ser verificados pelo Site Blindado 

PS: O modelo é uma simplificação do verdadeiro fluxo. Assim não assustamos tanto você =)

### Problema 2

> Um desenvolvedor começou a escrever um método, mas não está muito confiante sobre o que ele conseguiu desenvolver. O seu objetivo é analisar, propor mudanças e escrever o código. Esse método deve conseguir extrair informações do xml e tratar as informações

O método está no arquivo check_vulnerability.py

IMPORTANTE!
--------------------
Sua primeira tarefa será estipular um prazo para a conclusão do serviço!

### Bônus
* Se você tiver uma tendência para front, faça uma interface bacana (Bootstrap e/ou AngularJS serão muito bem vindos!)
* Se for backend, que tal criar uma API?

### Observações
* Precisa ser feito com python/django e javascript
* Pode usar plugins de terceiros
* Pesquise no Google a vontade